## MaterialSeekBarPreference


The best yet Implemetation of SeekBarPreference. Works on AndroidX and API >= 15

<img src="ART/screen_4.jpg" width="255">  

<img src="ART/screen_2.jpg" width="255">

<img src="ART/screen_3.jpg" width="255">

#Usage

Add this to your module dependencies:
```groovy
    compile 'com.rilixtech.widget:seekbarpreference:2.4.1'
````

Reference namespace on top of your layout file:
```xml
    xmlns:mseekbar="http://schemas.android.com/apk/res-auto">
````

Now you can use this view in your preferences layout, just like any other normal preference(API-v11+).
```xml
    <com.rilixtech.widget.seekbarpreference.SeekBarPreference
        android:key="your_pref_key"
        android:title="SeekbarPreference 2"
        android:summary="Some summary"
        android:enabled="false"
        android:defaultValue="5000"

        mseekbar:msbp_minValue="100"
        mseekbar:msbp_maxValue="10000"
        mseekbar:msbp_interval="200"
        mseekbar:msbp_measurementUnit="%"
        mseekbar:msbp_dialogEnabled="false"/>
````

If you have to support API-v7+, this lib provides also SeekBarPreferenceCompat that works with preference-v7.
```xml
    <com.rilixtech.widget.seekbarpreference.SeekBarPreferenceCompat
        android:key="your_pref_key"
        android:title="SeekbarPreference 2"
        android:summary="Some summary"
        android:enabled="false"
        android:defaultValue="5000"

        mseekbar:msbp_minValue="100"
        mseekbar:msbp_maxValue="10000"
        mseekbar:msbp_interval="200"
        mseekbar:msbp_measurementUnit="%"
        mseekbar:msbp_dialogEnabled="false"/>
````

Or use MaterialSeekBarView if you prefer to use views instead of preferences:
```xml
    <com.rilixtech.widget.seekbarpreference.SeekBarPreferenceView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"

        mseekbar:msbp_interval="200"
        mseekbar:msbp_maxValue="0"
        mseekbar:msbp_measurementUnit="bananas"
        mseekbar:msbp_minValue="-2000"
        mseekbar:msbp_dialogEnabled="false"

        mseekbar:msbp_view_title="SeekBarPreferenceView Example"
        mseekbar:msbp_view_summary="As you can see, view uses a bit different xml-attributes for some things"
        mseekbar:msbp_view_enabled="false"
        mseekbar:msbp_view_defaultValue="0" />
```

Either of way, View/Preference provides next methods to modify and manage it from Java:
```java
    public int getMaxValue();
    public void setMaxValue(int maxValue);

    public int getMinValue();
    public void setMinValue(int minValue);

    public String getTitle();
    public void setTitle(String title);

    public String getSummary();
    public void setSummary(String summary);

    public boolean isEnabled();
    public void setEnabled(boolean enabled);

    public int getInterval();
    public void setInterval(int interval);

    public int getCurrentValue();
    public void setCurrentValue(int currentValue);

    public String getMeasurementUnit();
    public void setMeasurementUnit(String measurementUnit);

    public void setDialogEnabled(boolean dialogEnabled);

    public void setDialogStyle(int dialogStyle);

    // AND for view-only(at least for now), there's a way to get a callback whenever value changes:
    public void setOnValueSelectedListener(PersistValueListener onValuePersisted);
```

As you can see, lib provides 4 universal custom attributes(msbp_minValue, msbp_maxValue, msbp_interval and msbp_measurementUnit).

There are also 4 additional attributes for view because it can't use corresponding ones from "android:" (msbp_view_title, msbp_view_summary, msbp_view_enabled and msbp_defaultValue)

Use them to define look and desired behavior.

Prefixes used to avoid attribute collisions with other libs.

# Thanks

* [Pavel Sikun](https://github.com/MrBIMC) the original version of this library.
* [krage](https://github.com/krage) for adding support for referenced resources.
* [NitroG42](https://github.com/NitroG42) for pointing out to attribute collisions.
* [Dmytro Karataiev](https://github.com/dmytroKarataiev) for a fix for defaultValue.
* [Mehmet Akif Tütüncü](https://github.com/mehmetakiftutuncu) for adding support to disable customInputDialog.

#Licence

       Copyright 2019 Rilix Technology  
       Copyright 2016 Pavel Sikun  
    
       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at
    
           http://www.apache.org/licenses/LICENSE-2.0
    
       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

