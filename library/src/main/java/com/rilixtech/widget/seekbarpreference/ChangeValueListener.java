package com.rilixtech.widget.seekbarpreference;

public interface ChangeValueListener {
  boolean onChange(int value);
}
