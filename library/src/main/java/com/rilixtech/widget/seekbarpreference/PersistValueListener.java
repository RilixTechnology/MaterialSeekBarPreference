package com.rilixtech.widget.seekbarpreference;

public interface PersistValueListener {
  boolean persistInt(int value);
}